class ApplicationController < ActionController::Base
  before_action :save_ip, :download_pdf


  def save_ip
    User.create(visitor_ip: request.remote_ip)
  end

  def download_pdf
    url = "https://drive.google.com/file/d/1Hd8Y1GMjVCq1pwDcEYxH5MaPkL5KbpmG/view?usp=sharing"
    redirect_to url
  end

end
